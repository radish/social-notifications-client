import QtQuick 2.5
import QtPositioning 5.5
import QtLocation 5.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Item {
    id: mapPlaceEvent
    width: 320
    visible: false
    anchors.rightMargin: 8
    anchors.bottomMargin: 0
    anchors.leftMargin: 8
    anchors.topMargin: 0
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.top: parent.top
    anchors.left: parent.left

    property alias isCoordinateChosen: buttonMapBack.visible
    property var coordinate

    Slider {
        id: mapZoomSlider;
        z: map.z + 1
        minimumValue: map.minimumZoomLevel;
        maximumValue: map.maximumZoomLevel;
        anchors.margins: 10
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.right: parent.right
        orientation : Qt.Vertical
        value: map.zoomLevel
        onValueChanged: {
            map.zoomLevel = value
        }
    }

    Plugin {
        id: osm
        name: "osm"
    }

    Button {
        id: buttonMapBack
        z: map.z + 1
        visible: false
        text: "Back"
        anchors.margins: 10
        anchors.bottomMargin: 20
        anchors.right: mapZoomSlider.left
        anchors.bottom: parent.bottom
        onClicked: {
            stack.pop()
        }
    }
    Map {
        anchors.fill: parent
        id: map
        plugin: osm
        zoomLevel: (maximumZoomLevel - minimumZoomLevel)/2
        center: QtPositioning.coordinate(52.408333, 16.934167)

        MouseArea {
            anchors.fill: parent
            function moveMarker(mouse) {
                markerItem.coordinate = map.toCoordinate(Qt.point(mouse.x, mouse.y))            }

            onPressAndHold: {
                map.gesture.enabled = false
                markerItem.coordinate = map.toCoordinate(Qt.point(mouse.x, mouse.y))
                markerItem.visible = true


                // we need to assign value field by field to not to trigger change of map.center
                positionChanged.connect(moveMarker)
            }
            onReleased: {
                positionChanged.disconnect(moveMarker)
                console.log("Testing stackget")
                console.log(stack.get(stack.depth-1,true))
                mainWindow.pointToPlace.latitude = markerItem.coordinate.latitude
                mainWindow.pointToPlace.longitude = markerItem.coordinate.longitude
                map.gesture.enabled = true
                mapPlaceEvent.isCoordinateChosen = true
                console.log(mainWindow.pointToPlace)
            }

        }
        MapQuickItem {
            id: markerItem
            coordinate: QtPositioning.coordinate(52.408333, 16.934167)
            anchorPoint.x: marker.width * 0.5
            anchorPoint.y: marker.height
            sourceItem:  Image {
                id: marker
                source: "qrc:///images/marker.svg"
            }
            visible: false

        }
    }
}
