import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtWebSockets 1.0

Item {
    id: loginPage
    anchors.rightMargin: 0
    anchors.bottomMargin: 0
    anchors.leftMargin: 0
    anchors.topMargin: 0
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.top: parent.top
    anchors.left: parent.left
    visible: true

    TextField {
        id: loginField
        x: 10
        y: 254
        height: 30
        z: 2
        anchors.leftMargin: 10
        anchors.left: parent.left
        anchors.rightMargin: 10
        anchors.right: parent.right
        placeholderText: qsTr("E-mail")
    }

    TextField {
        id: passwordField
        x: 10
        y: 332
        height: 30
        z: 2
        anchors.top: loginField.bottom
        anchors.topMargin: 6
        anchors.left: parent.left
        anchors.rightMargin: 10
        placeholderText: qsTr("Password")
        anchors.right: parent.right
        anchors.leftMargin: 10
        echoMode: 2
    }

    Image {
        id: imageLogo
        x: 0
        y: 47
        width: 320
        height: 191
        z: 2
        source: "images/logo.png"
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Button {
        id: buttonLogin
        x: 8
        y: 379
        width: 304
        height: 34
        text: qsTr("Login")
        anchors.bottom: buttonRegister.top
        anchors.bottomMargin: 6
        anchors.horizontalCenterOffset: 2
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Button {
        id: buttonRegister
        x: 8
        y: 385
        width: 304
        height: 34
        text: qsTr("Register")
        anchors.horizontalCenterOffset: 2
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Connections {
        target: buttonLogin
        onClicked: {
            var email = loginField.text
            var password = passwordField.text

            if (socket.active !== true) {
                console.log("Socket wasn't active")
                socket.active = false
                socket.active = true
                return
            }

            socket.login(email, password)
            console.log("Trying to login with " + email + " and " + "********")
        }
    }

    Connections {
        target: buttonRegister
        onClicked: {
            stack.push(Qt.resolvedUrl("RegisterPage.qml"))
        }
    }
}
