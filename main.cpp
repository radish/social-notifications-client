#include <QApplication>
#include <QQmlApplicationEngine>
#include <QDir>
#include "clientconnection.h"

#ifdef Q_OS_ANDROID
    #include "imagecapture_android.h"
#else
    #include "imagecapture.h"
#endif

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);


    // create cache directory for feeds' images
    QDir cache_dir = QDir(QDir::tempPath() + "/sps-cache");
    if ( !cache_dir.exists() ) {
        cache_dir.mkpath(".");
    }

    QQmlApplicationEngine engine;

    qmlRegisterType<ImageCapture>("ImageCapture", 1, 0, "ImageCapture");
    qmlRegisterType<ClientConnection>("ClientConnection", 1, 0, "ClientConnection");
    //qmlRegisterType<FeedElement>("FeedElement", 1, 0, "FeedElement");
    engine.load(QUrl(QStringLiteral("qrc:/sps.qml")));

    return app.exec();
}

