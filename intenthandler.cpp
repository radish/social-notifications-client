#include "intenthandler.h"
#include "imagecapture_android.h"
#include <QDebug>
#include <QtAndroidExtras/QAndroidJniEnvironment>

IntentHandler::IntentHandler(ImageCapture *ic)
    : m_ic(ic)
{

}

void IntentHandler::handleActivityResult(int receiverRequestCode, int resultCode, const QAndroidJniObject &data)
{
    qDebug() << "Hello";
    if (receiverRequestCode != 1337) {
        qDebug() << "Warning: We didn't expected intent with " << receiverRequestCode << " request code";
    } else {
        if (!data.isValid()) {
            qCritical() << "data is invalid";
        }

        QString file_path = QAndroidJniObject::getStaticObjectField<jstring>("net/putinf/QImageCapture", "mCurrentPhotoPath").toString();
        qDebug() << "result code: " << resultCode << ", path" << file_path;

        QAndroidJniEnvironment env;
        if (env->ExceptionCheck()) {
            qCritical() << "Exception occured!";
            env->ExceptionClear();
        }

        m_ic->set_m_path(file_path);
        emit m_ic->photoTaken();
    }
}

