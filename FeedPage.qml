import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtPositioning 5.3
import QtWebSockets 1.0

Item {
    id: feedPage
    width: 320
    visible: false
    anchors.rightMargin: 8
    anchors.bottomMargin: 0
    anchors.leftMargin: 8
    anchors.topMargin: 0
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.top: parent.top
    anchors.left: parent.left

    ColumnLayout {
        id: columnLayout1
        width: 320
        anchors.fill: parent

        ListView {
            id: feedView
            Layout.preferredHeight: 150
            boundsBehavior: Flickable.DragOverBounds
            highlightRangeMode: ListView.NoHighlightRange
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            highlightFollowsCurrentItem: true
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: mainWindow.listData
            delegate: Item {
                x: 5
                width: parent.width
                height: 110
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Clicked " + title)
                        stack.push({
                           "item": Qt.resolvedUrl("FeedShow.qml"),
                           "properties": {
                               "feedId"      : feedId,
                               "title"       : title,
                               "description" : description,
                               "photo"       : photo,
                               "author"      : author,
                               "coordinate"  : QtPositioning.coordinate(latitude, longitude),
                               "time"        : time
                           }
                       })
                    }
                }

                Rectangle {
                    anchors.margins: 8
                    width: parent.width
                    height: 100
                    border.width: 1
                    border.color: "#888888"
                    color: "#ececec"
                    radius:8
                }

                Row {
                    id: row1
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    y: 4
                    x: 4
                    Text {
                        text: rating
                        font.bold: true
                        font.pointSize: 20
                        verticalAlignment: Text.AlignVCenter
                        color: (rating < 0) ? "#b2b2b2" : "#000000"
                        anchors.verticalCenter: parent.verticalCenter
                        //Layout.rowSpan: 2
                    }

                    Image {
                        width: 90
                        height: 90
                        source: photo
                    }

                    Text {
                        text: "<h3>" + title + "</h3>" + ((description.length > 100) ? description.substring(0,100) + "..." : description)
                        wrapMode: Text.Wrap
                        font.pointSize: 9
                        textFormat: Text.StyledText
                        verticalAlignment: Text.AlignVCenter
                        width: parent.parent.width - 30
                        color: (rating < 0) ? "#b2b2b2" : "#000000"
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    spacing: 10
                }
            }

        }



        GridLayout {
            id: gridLayout1
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            anchors.verticalCenterOffset: 187
            anchors.leftMargin: 0
            anchors.rightMargin: 0
            Layout.fillHeight: false
            Layout.fillWidth: true
            rows: 2
            columns: 2


            Button {
                id: buttonNotifyOthers
                text: qsTr("Notify others")
                Layout.fillWidth: true
            }

            Button {
                id: buttonRefreshFeeds
                text: qsTr("Refresh")
                Layout.fillWidth: true
            }
        }
    }

    Connections {
        target: buttonNotifyOthers
        onClicked: {
            stack.push(Qt.resolvedUrl("FeedCreate.qml"))
        }
    }

    Connections {
        target: buttonRefreshFeeds
        onClicked: {
            socket.refresh_feeds()
        }
    }

}
