import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtWebSockets 1.0
import QtPositioning 5.3
import ClientConnection 1.0
import "sps.js" as Sps

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 320
    height: 480
    title: qsTr("Hello World")

    property var subscribePoint: QtPositioning.coordinate(52.408333, 16.934167)
    property var pointToPlace: QtPositioning.coordinate(52.408333, 16.934167)
    property real subscribeRadius: 1000.0
    property alias listData: listData
    property alias msgDialog: msgDialog

    StackView {
        id: stack
        anchors.fill: parent
        initialItem: Qt.resolvedUrl("LoginPage.qml")
        delegate: StackViewDelegate {
            pushTransition: StackViewTransition {
                PropertyAnimation {
                    target: enterItem
                    property: "opacity"
                    from: 0
                    to: 1
                }
                PropertyAnimation {
                    target: exitItem
                    property: "opacity"
                    from: 1
                    to: 0
                }
            }

            popTransition: StackViewTransition {
                PropertyAnimation {
                    target: enterItem
                    property: "opacity"
                    from: 0
                    to: 1
                }
                PropertyAnimation {
                    target: exitItem
                    property: "opacity"
                    from: 1
                    to: 0
                }
            }

            replaceTransition: StackViewTransition {
                PropertyAnimation {
                    target: enterItem
                    property: "opacity"
                    from: 0
                    to: 1
                }
                PropertyAnimation {
                    target: exitItem
                    property: "opacity"
                    from: 1
                    to: 0
                }
            }
        }
    }

    menuBar: MenuBar {
        id: menu
        property alias isLogged: menuLogout.enabled
        Menu {
            title: "Menu"
            MenuItem {
                text: "FeedPage"
                onTriggered: {
                    console.log("Changing to feedPage");
                    stack.replace(Qt.resolvedUrl("FeedPage.qml"))
                }
            }
            MenuItem {
                text: "StackTest"
                onTriggered: {
                    console.log("Checking stack");
                    console.log(Stack.view)
                   Stack.view.h(function (rzecz, index) {console.log(rzecz.subscribePoint)})
                }
            }
            MenuItem {
                text: "Subscribe"
                onTriggered: {
                    console.log("Changing to mapSubscribe");

                    // assigning subscribePoint to subscribePoint trigger map to center
                    subscribePoint = subscribePoint
                    stack.push({
                        "item": Qt.resolvedUrl("MapSubscribe.qml"),
                        "properties": {
                            "circleCoordinate": mainWindow.subscribePoint,
                            "circleRadius": mainWindow.subscribeRadius
                         }
                    })
                }
            }
            MenuItem {
                text: "Reconnect"
                onTriggered: {
                    console.log("Reconnecting...")
                    socket.active = false
                    socket.active = true
                }
            }

//            MenuSeparator {
//                visible: true
//            }

            MenuItem {
                id: menuLogout
                text: "Logout"
                enabled: false
                onTriggered: {
                    console.log("Logging out...");
                    enabled = false
                    stack.pop(null)
                    stack.replace(Qt.resolvedUrl("LoginPage.qml"))
                    socket.active = false
                    socket.active = true
                }
            }


            MenuItem {
                text: "Exit"
                onTriggered: Qt.quit();
            }
        }
    }


    ClientConnection {
        id: socket
        url: "ws://127.0.0.1:8000"
        active: true
        onConnected: console.log("Hello my server")
        onDisconnected: console.log("Goodbye my friend")
        onLoginSuccess: {
            stack.push(Qt.resolvedUrl("FeedPage.qml"))
            menu.isLogged = true
            socket.subscribeArea(mainWindow.subscribePoint, mainWindow.subscribeRadius)
            socket.refresh_feeds()

        }
        onLoginFailure: {
            msgDialog.show("LOGIN FAILED: " + reason)
        }
        onSignupSuccess: {
            stack.pop()
            msgDialog.show("Registeration completed successfully!")
        }
        onSignupFailure: {
            msgDialog.show("REGISTER FAILED " + reason)
        }
        onClearFeeds: {
            listData.clear()
        }

        onNotifySuccess: {
            stack.pop()
            msgDialog.show("Notify sended successfully!")
        }

        onNotifyFailure: {
            msgDialog.show("NOTIFY SENDING FAILED: " + reason)
        }

        onUpvoteSuccess: {
            msgDialog.show("Upvoted successfully!")
        }

        onUpvoteFailure: {
            msgDialog.show("UPVOTE FAILED: " + reason)
        }

        onDownvoteSuccess: {
            msgDialog.show("Downvoted successfully!")
        }

        onDownvoteFailure: {
            msgDialog.show("DOWNVOTE FAILED: " + reason)
        }

        onNewFeed: {
            console.log("Yay, new feeds! ")
            listData.append({
                "feedId": feedId,
                "title": title,
                "description": description,
                "photo": photo,
                "latitude": coordinate.latitude,
                "longitude": coordinate.longitude,
                "author": author,
                "time": time.toLocaleString(),
                "rating": rating
            })
        }
        onUpdatedFeed: {
            for (var i = 0; i < listData.count; i++) {
                var element = listData.get(i)
                if (element.feedId === feedId) {
                    element.title = title,
                    element.description = description,
                    element.photo = photo,
                    element.latitude = coordinate.latitude,
                    element.longitude = coordinate.longitude,
                    element.author = author,
                    element.time = time.toLocaleString(),
                    element.rating = rating
                    console.log("Updated feed " + title)
                    break;
                }
            }
        }

        onError: {
            console.log("Server connection error occured: " + socket.errorString());
        }
    }



    ListModel {
        id: listData
//            ListElement {
//                feedId: "asdfaadsfasdf"
//                title: "Piesel"
//                description: "Sasdfjjjjjjjjj     asdffffff lkjq erq lwkj  qwerl;kj  qwer jjjjjjjjjjuper piesel Sasdfjjjjjjjjj     asdffffff lkjq erq lwkj  qwerl;kj  qwer jjjjjjjjjjuper piesel"
//                photo: "file:///tmp/pies.jpg"
//                author: "Autor"
//            }
    }


    MessageDialog {
        id: msgDialog
        title: "SPS"

        function show(caption) {
            msgDialog.text = caption;
            msgDialog.open();

        }
    }

//    ApplicationWindow {
//        id: settingsWindow
//        title: qsTr("Settings")
//
//        function show() {
//        }
//
//    }
}


