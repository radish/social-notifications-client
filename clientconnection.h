#ifndef CLIENTCONNECTION_H
#define CLIENTCONNECTION_H

#include <QQuickItem>
#include <QSharedPointer>
#include <QtWebSockets/QtWebSockets>
#include <QGeoCoordinate>
#include "feedelement.h"



#include <QString>

class ClientConnection : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString url READ getUrl WRITE setURL NOTIFY urlChanged)
    Q_PROPERTY(bool active READ isActive WRITE setActive NOTIFY activeChanged)

public:
    explicit ClientConnection(QObject *parent = 0);
    Q_INVOKABLE void login(QString name, QString password);
    Q_INVOKABLE void signup(QString name, QString surname, QString username, QString email, QString password);
    Q_INVOKABLE void subscribeArea(QGeoCoordinate point, double radius);
    Q_INVOKABLE void notify_others(QString title, QString description, QString photo, QGeoCoordinate coordinate);
    Q_INVOKABLE void upvote_feed(QString feedId);
    Q_INVOKABLE void downvote_feed(QString feedId);
    Q_INVOKABLE void refresh_feeds();
    Q_INVOKABLE QString errorString() const;

    const QString& getUrl() const;
    void setURL(const QString& new_url);

    bool isActive() const;
    void setActive(bool is_active);

private:
    QWebSocket webSocket;
    QString    m_url;
    bool       m_active;

    QGeoCoordinate extractCoordinate(const QVariantMap& data);
    QDateTime extractTime(const QVariantMap& data);

signals:
    void connected();
    void disconnected();
    void error();
    void binaryMessageReceived(const QByteArray &message);
    void loginSuccess();
    void loginFailure(const QString& reason);
    void signupSuccess();
    void signupFailure(const QString& reason);
    void notifySuccess();
    void notifyFailure(const QString& reason);
    void upvoteSuccess();
    void upvoteFailure(const QString& reason);
    void downvoteSuccess();
    void downvoteFailure(const QString& reason);
    void urlChanged(const QString& new_url);
    void activeChanged(const bool is_active);
    void newFeed(const QString& feedId, const QString& title, const QString& description, const QString& photo, QGeoCoordinate coordinate, const QString& author, const QDateTime& time, int rating);
    void updatedFeed(const QString& feedId, const QString& title, const QString& description, const QString& photo, QGeoCoordinate coordinate, const QString& author, const QDateTime& time, int rating);
    void clearFeeds();

private slots:
    void processBinaryMessage(const QByteArray &message);

};

inline const QString &ClientConnection::getUrl() const
{
    return m_url;
}

inline void ClientConnection::setURL(const QString &new_url)
{
    if (m_url != new_url) {
        // if m_url is empty and connection is active,
        // it means that connection couldn't be opened, because m_url hadn't been initialized
        if (m_url.isEmpty() && !new_url.isEmpty() && m_active == true) {
            webSocket.open(new_url);
            qDebug() << "Connecting to " << new_url << "...";
        }

        m_url = QString(new_url);
        emit urlChanged(new_url);

    }
}

inline bool ClientConnection::isActive() const
{
    return m_active;
}

inline void ClientConnection::setActive(bool is_active)
{
    if (m_active != is_active) {
        m_active = is_active;
        emit activeChanged(is_active);

        if (is_active && !m_url.isEmpty()) {
            webSocket.open(m_url);
            qDebug() << "Connecting to " << m_url << " ...";
        } else {
            QAbstractSocket::SocketState state = webSocket.state();
            if (state == QAbstractSocket::HostLookupState || state == QAbstractSocket::ConnectingState
                    || state == QAbstractSocket::ConnectedState) {
                webSocket.close();
            }
        }
    }
}

inline QGeoCoordinate ClientConnection::extractCoordinate(const QVariantMap &data)
{
    QVariantList coords = data.value("point").toMap().value("coordinates").toList();
    return QGeoCoordinate(coords.at(1).toDouble(), coords.at(0).toDouble());
}

inline QDateTime ClientConnection::extractTime(const QVariantMap &data)
{
    return QDateTime::fromTime_t(static_cast<uint>(data.value("time").toMap().value("epoch_time").toDouble()));
}

inline QString ClientConnection::errorString() const
{
    return webSocket.errorString();
}

#endif // CLIENTCONNECTION_H
