TEMPLATE = app

QT += qml quick widgets websockets location positioning

SOURCES += main.cpp \
    clientconnection.cpp \
    feedelement.cpp

RESOURCES += qml.qrc

LIBS += $$PWD/lib/qmsgpack/bin/libqmsgpack.so

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# pass arguments to compiler
QMAKE_CXXFLAGS += -O2 -std=c++11

!android {
    HEADERS += imagecapture.h
    SOURCES += imagecapture.cpp
}

android {
    QT +=  androidextras
    HEADERS += intenthandler.h \
        imagecapture_android.h

    SOURCES += intenthandler.cpp \
        imagecapture_android.cpp
    OTHER_FILES += \
        android-java/src/net/putinf/QImageCapture.java \
        android-java/libs/armeabi/libqmsgpack.so
        android-java/libs/armeabi/libQt5Location.so
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android-java
}


# Default rules for deployment.
include(deployment.pri)

DISTFILES +=

HEADERS += \
    clientconnection.h \
    feedelement.h

# INCLUDEPATH += $$PWD/lib/msgpack-c/include \
INCLUDEPATH += $$PWD/lib/qmsgpack/src
