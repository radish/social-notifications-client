import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtPositioning 5.3
import ImageCapture 1.0
import QtTest 1.1

Item {
    id: feedCreate
    width: 320
    visible: false
    anchors.rightMargin: 8
    anchors.bottomMargin: 0
    anchors.leftMargin: 8
    anchors.topMargin: 0
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.top: parent.top
    anchors.left: parent.left

    property bool isCoordinateChosen: false
    property var coordinate: QtPositioning.coordinate(52.408333, 16.934167)


    ImageCapture {
        id: takePhoto
    }

    SignalSpy {
        id: spytakePhoto
        target: takePhoto
        signalName: "photoTaken"
    }

    ColumnLayout {
        id: columnLayout1
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent


        TextField {
            id: fieldFeedTitle
            placeholderText: qsTr("Title")
            Layout.fillWidth: true
        }
        TextArea {
            id: fieldFeedDescription
            text: qsTr("Description...")
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        GridLayout {
            columns: 2
            Layout.fillWidth: true
            Button {
                id: buttonPhoto
                text: qsTr("Photo")
                Layout.fillWidth: true
            }

            Button {
                id: buttonCoordinate
                text: qsTr("Coordinate")
                Layout.fillWidth: true
            }

            Button {
                id: buttonFeedCreateBack
                text: qsTr("Cancel")
                Layout.fillWidth: true
            }

            Button {
                id: buttonSendMessage
                text: qsTr("Send!")
                Layout.fillWidth: true
            }

        }

    }

    Connections {
        target: buttonPhoto
        onClicked: {
            spytakePhoto.clear()
            takePhoto.share()
            if (spytakePhoto.count === 0) {
                spytakePhoto.wait(30000)
            }
            console.log("Image taken: " + takePhoto.path);
        }
    }


    Connections {
        target: buttonCoordinate
        onClicked: {
            console.log(mainWindow)
            console.log(feedCreate)
            stack.push({
                "item": Qt.resolvedUrl("MapPlaceEvent.qml"),
            })
            feedCreate.isCoordinateChosen = true
            console.log("Placed coord: " + mainWindow.pointToPlace)
        }
    }
    Connections {
        target: buttonSendMessage
        onClicked: {
            if (feedCreate.isCoordinateChosen == false) {
                stack.push(Qt.resolvedUrl("MapPlaceEvent.qml"))
                feedCreate.isCoordinateChosen = true
            } else {
                socket.notify_others(fieldFeedTitle.text, fieldFeedDescription.text, takePhoto.path, mainWindow.pointToPlace)
            }
        }
    }

    Connections {
        target: buttonFeedCreateBack
        onClicked: {
            stack.pop()
        }
    }
}
