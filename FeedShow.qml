import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2


Item {
    id: feedShow
    width: 320
    visible: false
    anchors.rightMargin: 8
    anchors.bottomMargin: 0
    anchors.leftMargin: 8
    anchors.topMargin: 0
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.top: parent.top
    anchors.left: parent.left

    property string feedId
    property alias photo: notification_image.source
    property string title
    property string description
    property var coordinate
    property string time
    property string author




    Flickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: contentItem.childrenRect.height

        ColumnLayout {
            id: columnLayout2

            Image {
               id: notification_image
               Layout.preferredWidth: feedShow.width
               Layout.preferredHeight: paintedHeight
               fillMode: Image.PreserveAspectFit
               source: ""
               onStatusChanged: {
                   if (notification_image.status == Image.Null) {
                       console.log("Img status: Null")
                   }

                   if (notification_image.status == Image.Ready) {
                       console.log("Img status: Ready")
                   }

                   if (notification_image.status == Image.Loading) {
                       console.log("Img status: Loading")
                   }

                   if (notification_image.status == Image.Error) {
                       console.log("Img status: Error")
                   }

               }
            }

            Text {
                text: "<h1>" + feedShow.title + "</h1><font color=\"#b2b2b2\"><i>Added by " + author + "<br>" + time + "</i></font>"
                Layout.maximumWidth: feedShow.width
                wrapMode: TextEdit.WordWrap
                textFormat: Text.StyledText
            }
            Text {
                Layout.maximumWidth: feedShow.width
                wrapMode: TextEdit.WordWrap
                text: feedShow.description
            }

            GridLayout {
                columns:2
                Layout.maximumWidth: 320 - anchors.leftMargin - anchors.rightMargin
                Button {
                    id: buttonFeedShowUpvote
                    text: qsTr("Upvote")
                    Layout.fillWidth: true
                }
                Button {
                    id: buttonFeedShowDownvote
                    text: qsTr("Downvote")
                    Layout.fillWidth: true
                }
                Button {
                    id: buttonFeedShowBack
                    text: qsTr("Back to feed")
                    Layout.fillWidth: true
                }
                Button {
                    id: buttonFeedShowMap
                    text: qsTr("Show me!")
                    Layout.fillWidth: true
                }
            }
        }
    }


    Connections {
        target: buttonFeedShowBack
        onClicked: {
            stack.pop()
        }
    }

    Connections {
        target: buttonFeedShowMap
        onClicked: {
            console.log("Showing on map " + coordinate)
            stack.push({
                 "item": Qt.resolvedUrl("MapShowEvent.qml"),
                 "properties": {
                     "coordinate": coordinate
                 }
            })
        }
    }

    Connections {
        target: buttonFeedShowUpvote
        onClicked: {
            console.log("Upvote " + feedShow.feedId)
            socket.upvote_feed(feedShow.feedId)
        }
    }

    Connections {
        target: buttonFeedShowDownvote
        onClicked: {
            console.log("Downvote " + feedShow.feedId)
            socket.downvote_feed(feedShow.feedId)
        }
    }
}
