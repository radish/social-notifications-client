#include <QAndroidActivityResultReceiver>
#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QtAndroid>
#include <QThread>
#include <QDebug>

#include "imagecapture_android.h"
#include "intenthandler.h"



ImageCapture::ImageCapture(QObject *parent)
    : QObject(parent), resultReceiver(this)
{
    //QObject::connect(&resultReceiver, &IntentHandler::intentCompleted, this, &ImageCapture::onIntentCompleted);
}

QString ImageCapture::share()
{
    qDebug() << "IC: Hello, Image Capture method";
    m_path = QString();
    QAndroidJniObject takePictureIntent = QAndroidJniObject::callStaticObjectMethod("net/putinf/QImageCapture", "create_intent", "()Landroid/content/Intent;");

    if (!takePictureIntent.isValid()) {
        qCritical() << "takePictureIntent is invalid";
    }

    QtAndroid::startActivity(takePictureIntent, 1337, &resultReceiver);
    QAndroidJniEnvironment env;
    if (env->ExceptionCheck()) {
        qCritical() << "Exception occured!";
        env->ExceptionClear();
    }
    //while (resultReceiver.getImagePath().isEmpty()) {
    //    QThread::currentThread()->sleep(5);
    //    qDebug() << "IC: still wating...";
    //};

    return "intent requested";
}

void ImageCapture::set_m_path(QString url)
{
    m_path = url;
}
