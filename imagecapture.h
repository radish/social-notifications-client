#ifndef IMAGECAPTURE_H
#define IMAGECAPTURE_H

#include <QString>
#include <QQuickItem>

class ImageCapture : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString path MEMBER m_path NOTIFY pathChanged)

public:
    explicit ImageCapture(QQuickItem *parent = 0);
    Q_INVOKABLE QString share();

private:
    QString m_path;

signals:
    void pathChanged();
    void photoTaken();
};

#endif // IMAGECAPTURE_H
