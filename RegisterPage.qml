import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtWebSockets 1.0

Item {
    anchors.fill: parent
    anchors.rightMargin: -1
    anchors.bottomMargin: 0
    anchors.leftMargin: 1
    anchors.topMargin: 0
    visible: false

    GridLayout {
        id: gridLayout2

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.leftMargin: 8
        anchors.bottomMargin: 5
        anchors.topMargin: 0
        columns: 2

        Label {
            id: label1
            text: qsTr("Name:")
        }

        TextField {
            id: fieldRegisterName
            placeholderText: qsTr("")
            Layout.fillWidth: true
        }

        Label {
            id: label2
            text: qsTr("Surname:")
        }


        TextField {
            id: fieldRegisterSurname
            placeholderText: qsTr("")
            Layout.fillWidth: true
        }



        Label {
            id: label3
            text: qsTr("Username:")
        }

        TextField {
            id: fieldRegisterUsername
            placeholderText: qsTr("")
            Layout.fillWidth: true
        }

        Label {
            id: label4
            text: qsTr("Email:")
        }

        TextField {
            id: fieldRegisterEmail
            placeholderText: qsTr("")
            Layout.fillWidth: true
        }

        Label {
            id: label5
            text: qsTr("Password:")
            opacity: 1
        }

        TextField {
            id: fieldRegisterPassword
            placeholderText: qsTr("")
            Layout.fillWidth: true
            echoMode: 2
        }

        Button {
            id: buttonRegisterFinish
            text: qsTr("Register")
            Layout.columnSpan: 2
            Layout.rowSpan: 1
            Layout.fillWidth: true
        }

        Button {
            id: button1
            text: qsTr("Nope")
        }
    }

    Connections {
        target: button1
        onClicked: {
            stack.pop()
        }
    }

    Connections {
        target: buttonRegisterFinish
        onClicked: {
            var name = fieldRegisterName.text
            var surname = fieldRegisterSurname.text
            var username = fieldRegisterUsername.text
            var email = fieldRegisterEmail.text
            var passwd = fieldRegisterPassword.text

            if (socket.active !== true) {
                console.log("Socket wasn't active")
                socket.active = false
                socket.active = true
                return
            }

            console.log("Trying to register " + email + "...")
            socket.signup(name, surname, username, email, passwd)
        }
    }
}
