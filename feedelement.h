#ifndef FEEDELEMENT_H
#define FEEDELEMENT_H

#include <QObject>
#include <QString>
#include <QGeoCoordinate>


class FeedElement : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString feedId MEMBER m_feedId NOTIFY feedIdChanged)
    Q_PROPERTY(QString title MEMBER m_title NOTIFY titleChanged)
    Q_PROPERTY(QString description MEMBER m_description NOTIFY descriptionChanged)
    Q_PROPERTY(QString photo MEMBER m_photo NOTIFY photoChanged)
    Q_PROPERTY(QGeoCoordinate point MEMBER m_point NOTIFY pointChanged)
    Q_PROPERTY(QString author MEMBER m_author NOTIFY authorChanged)

public:
    explicit FeedElement(QObject *parent = 0);
    explicit FeedElement(QString feedId, QString title, QString description, QString photo, QGeoCoordinate point, QString author, QObject *parent = 0);

private:
    QString m_feedId;
    QString m_title;
    QString m_description;
    QString m_photo;
    QGeoCoordinate m_point;
    QString m_author;

signals:
    void feedIdChanged();
    void titleChanged();
    void descriptionChanged();
    void photoChanged();
    void pointChanged();
    void authorChanged();
};

#endif // FEEDELEMENT_H
