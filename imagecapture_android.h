﻿#ifndef IMAGECAPTURE_ANDROID_H
#define IMAGECAPTURE_ANDROID_H


#include <QString>
#include <QObject>
#include "intenthandler.h"

class ImageCapture : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString path MEMBER m_path NOTIFY pathChanged)
public:
    explicit ImageCapture(QObject *parent = 0);
    Q_INVOKABLE QString share();
    IntentHandler resultReceiver;
    void set_m_path(QString url);

private:
    QString m_path;

signals:
    void pathChanged();
    void photoTaken();

};

#endif // IMAGECAPTURE_ANDROID_H
