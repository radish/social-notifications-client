﻿#include "clientconnection.h"
#include <msgpack.h>
#include <QVariant>
#include <QString>

ClientConnection::ClientConnection(QObject *parent)
    : QObject(parent), m_active(false)
{
    QObject::connect(&webSocket,
                     static_cast<void (QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error),
                     this,
                     &ClientConnection::error);
    QObject::connect(&webSocket, &QWebSocket::connected, this, &ClientConnection::connected);
    QObject::connect(&webSocket, &QWebSocket::disconnected, this, &ClientConnection::disconnected);
    QObject::connect(&webSocket, &QWebSocket::binaryMessageReceived, this, &ClientConnection::binaryMessageReceived);
    QObject::connect(&webSocket, &QWebSocket::binaryMessageReceived, this, &ClientConnection::processBinaryMessage);
}

void ClientConnection::login(QString name, QString password)
{
    QVariantMap login_msg {
        {"action", "login"},
        {"data", QVariantMap {
            {"email", name},
            {"password", password}
        }}
    };

    QByteArray msg_to_send = MsgPack::pack(login_msg);
    webSocket.sendBinaryMessage(msg_to_send);
}

void ClientConnection::subscribeArea(QGeoCoordinate point, double radius)
{

    // [[x,y], range]
    QVariantList circle_cords;

    QVariantList gps_point;
    gps_point.append(point.longitude());
    gps_point.append(point.latitude());

    QVariantMap msg {
        {"action", "subscribe"},
        {"data", QVariantList {
                QVariantList { point.longitude(), point.latitude()},
                radius
            }
        }
    };

    qDebug() << msg;

    QByteArray msg_to_send = MsgPack::pack(msg);
    webSocket.sendBinaryMessage(msg_to_send);
}

void ClientConnection::signup(QString name, QString surname, QString username, QString email, QString password)
{
    QVariantMap register_msg {
        {"action", "register"},
        {"data", QVariantMap {
            {"name", name},
            {"surname", surname},
            {"username", username},
            {"email", email},
            {"password", password}
        }}
    };

    QByteArray msg_to_send = MsgPack::pack(register_msg);
    webSocket.sendBinaryMessage(msg_to_send);
}

void ClientConnection::notify_others(QString title, QString description, QString photo, QGeoCoordinate coordinate)
{
    // read image as binary data
    //QImage image(photo.toLocalFile());
    bool skip_photo = false;
    QByteArray image;
    if (photo.isEmpty()) {
        skip_photo = true;
    } else {
        QFile image_file(photo);
        if (!image_file.open(QIODevice::ReadOnly)) {
            skip_photo = true;
        } else {
            image = image_file.readAll();
        }
    }

    QVariantList gps_point;
    gps_point.append(coordinate.longitude());
    gps_point.append(coordinate.latitude());

    if (!skip_photo) {
        QVariantMap msg {
            {"action", "add_notification"},
            {"data", QVariantMap {
                    {"title", title},
                    {"description", description},
                    {"photo", image},
                    {"point", gps_point}
                }}
        };
        QByteArray msg_to_send = MsgPack::pack(msg);

        webSocket.sendBinaryMessage(msg_to_send);
    } else {
        QVariantMap msg {
            {"action", "add_notification"},
            {"data", QVariantMap {
                    {"title", title},
                    {"description", description},
                    {"point", gps_point}
                }}
        };
        QByteArray msg_to_send = MsgPack::pack(msg);
        webSocket.sendBinaryMessage(msg_to_send);
    }
}

void ClientConnection::upvote_feed(QString feedId)
{
        QVariantMap msg {
            {"action", "vote_up"},
            {"data", feedId}
        };
        QByteArray msg_to_send = MsgPack::pack(msg);
        webSocket.sendBinaryMessage(msg_to_send);
}

void ClientConnection::downvote_feed(QString feedId)
{
        QVariantMap msg {
            {"action", "vote_down"},
            {"data", feedId}
        };
        QByteArray msg_to_send = MsgPack::pack(msg);
        webSocket.sendBinaryMessage(msg_to_send);
}

void ClientConnection::refresh_feeds()
{
    emit clearFeeds();
    QVariantMap msg {
        {"action", "get_notifications"}
    };

    QByteArray msg_to_send = MsgPack::pack(msg);
    webSocket.sendBinaryMessage(msg_to_send);

}



void ClientConnection::processBinaryMessage(const QByteArray &message)
{
    QVariantMap response = MsgPack::unpack(message).toMap();

    qDebug() << "Bin msg received: " << response;
    QString response_type = response.value("action").toString();
    if (response_type.isEmpty()) {
        qDebug() << "Warning: received empty response_type";
        return;
    } else if (response_type == "login") {
        if (!response.contains("result")) {
            qDebug() << "Warning: received empty result";
            return;
        } else {
            bool success = response.value("result").toBool();
            if (success) {
                emit loginSuccess();
            } else {
                emit loginFailure(response.value("reason").toString());
            }
            return;
        }
    } else if (response_type == "register") {
        bool success = response.value("result").toBool();
        if (success) {
            emit signupSuccess();
        } else {
            emit signupFailure(response.value("reason").toString());
        }
        return;
    } else if (response_type == "get_notifications") {
        if (!response.contains("data")) {
            qDebug() << "Warning: received empty data";
            return;
        } else {
            // save images to files in /tmp/sps-cache/
            QVariantList data = response.value("data").toList();
            foreach (const QVariant& datum, data) {
                QVariantMap element = datum.toMap();
                if (!element.contains("id") || element.value("id").toString().isEmpty()) {
                    qDebug() << "Warning: invalid id, skipping element";
                    continue;
                }

                QByteArray image = element.value("photo").toByteArray();
                QFile image_out(QDir::tempPath() + "/sps-cache/" + element.value("id").toString() + ".jpg");
                if (!image_out.open(QIODevice::WriteOnly)) {
                    qDebug() << "Warning: can't open file to write image";
                }
                image_out.write(image);
            }

            // emit info and data about new feeds
            foreach (const QVariant& datum, data) {
                QVariantMap element = datum.toMap();
                QString feedId = element.value("id").toString();
                QString title = element.value("title").toString();
                QString description = element.value("description").toString();
                QString photo = "file://" + QDir::tempPath() + "/sps-cache/" + element.value("id").toString() + ".jpg";
                QGeoCoordinate coordinate = extractCoordinate(element);
                QString author = element.value("author").toString();
                QDateTime time = extractTime(element);
                int rating = element.value("rating").toInt();
                emit newFeed(feedId, title, description, photo, coordinate, author, time, rating);
            }
            return;
        }
    } else if (response_type == "new_notification") {
        if (!response.contains("data")) {
            qDebug() << "Warning: received empty data";
            return;
        } else {
            // save image to files in /tmp/sps-cache/
            QVariantMap data = response.value("data").toMap();
            if (!data.contains("id") || data.value("id").toString().isEmpty()) {
                qDebug() << "Warning: invalid id, skipping element";
                return;
            }

            QByteArray image = data.value("photo").toByteArray();
            QFile image_out(QDir::tempPath() + "/sps-cache/" + data.value("id").toString() + ".jpg");
            if (!image_out.open(QIODevice::WriteOnly)) {
                qDebug() << "Warning: can't open file to write image";
            }

            image_out.write(image);
            QString feedId = data.value("id").toString();
            QString title = data.value("title").toString();
            QString description = data.value("description").toString();
            QString photo = "file://" + QDir::tempPath() + "/sps-cache/" + data.value("id").toString() + ".jpg";
            QGeoCoordinate coordinate = extractCoordinate(data);
            QString author = data.value("author").toString();
            QDateTime time = extractTime(data);
            int rating = data.value("rating").toInt();

            emit newFeed(feedId, title, description, photo, coordinate, author, time, rating);

            return;
        }
    } else if (response_type == "updated_notification") {
        if (!response.contains("data")) {
            qDebug() << "Warning: received empty data";
            return;
        } else {
            // save image to files in /tmp/sps-cache/
            QVariantMap data = response.value("data").toMap();
            if (!data.contains("id") || data.value("id").toString().isEmpty()) {
                qDebug() << "Warning: invalid id, skipping element";
                return;
            }

            QByteArray image = data.value("photo").toByteArray();
            QFile image_out(QDir::tempPath() + "/sps-cache/" + data.value("id").toString() + ".jpg");
            if (!image_out.open(QIODevice::WriteOnly)) {
                qDebug() << "Warning: can't open file to write image";
            }

            image_out.write(image);
            QString feedId = data.value("id").toString();
            QString title = data.value("title").toString();
            QString description = data.value("description").toString();
            QString photo = "file://" + QDir::tempPath() + "/sps-cache/" + data.value("id").toString() + ".jpg";
            QGeoCoordinate coordinate = extractCoordinate(data);
            QString author = data.value("author").toString();
            QDateTime time = extractTime(data);
            int rating = data.value("rating").toInt();

            emit updatedFeed(feedId, title, description, photo, coordinate, author, time, rating);
        }
        return;

    } else if (response_type == "add_notification") {
            bool success = response.value("result").toBool();
            if (success) {
                emit notifySuccess();
            } else {
                emit notifyFailure(response.value("reason").toString());
            }
            return;
    } else if (response_type == "vote_up"){
            bool success = response.value("result").toBool();
            if (success) {
                emit upvoteSuccess();
            } else {
                emit upvoteFailure(response.value("reason").toString());
            }
            return;
    } else if (response_type == "vote_down"){
            bool success = response.value("result").toBool();
            if (success) {
                emit downvoteSuccess();
            } else {
                emit downvoteFailure(response.value("reason").toString());
            }
            return;
    } else {
        qDebug() << "Warning: invalid response type from server";
    }
}

