import QtQuick 2.5
import QtPositioning 5.5
import QtLocation 5.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Item {
    id: mapSubscribe
    width: 320
    visible: false
    anchors.rightMargin: 8
    anchors.bottomMargin: 0
    anchors.leftMargin: 8
    anchors.topMargin: 0
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.top: parent.top
    anchors.left: parent.left

    Slider {
        id: mapZoomSlider;
        z: map.z + 1
        minimumValue: map.minimumZoomLevel;
        maximumValue: map.maximumZoomLevel;
        anchors.margins: 10
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.right: parent.right
        orientation : Qt.Vertical
        value: map.zoomLevel
        onValueChanged: {
            map.zoomLevel = value
        }
    }

    Plugin {
        id: osm
        name: "osm"
    }

    Button {
        id: buttonMapBack
        z: map.z + 1
        text: qsTr("Accept")
        anchors.margins: 10
        anchors.bottomMargin: 20
        anchors.right: mapZoomSlider.left
        anchors.bottom: parent.bottom
        onClicked: {
            stack.pop()
        }
    }
    Map {
        anchors.fill: parent
        id: map
        plugin: osm
        zoomLevel: (maximumZoomLevel - minimumZoomLevel)/2
        center: mainWindow.subscribePoint

        MouseArea {
            property bool isClicked: false
            anchors.fill: parent

            function resizeCircle(mouse) {
                circle.radius = circle.center.distanceTo(map.toCoordinate(Qt.point(mouse.x, mouse.y)))
            }

            onPressAndHold: {
                map.gesture.enabled = false
                console.log("Long press: " + mouse.x + " " + mouse.y)
                circle.center = map.toCoordinate(Qt.point(mouse.x, mouse.y))
                circle.visible = true
                // we need to assign value field by field to not to trigger change of map.center
                mainWindow.subscribePoint.latitude = circle.center.latitude
                mainWindow.subscribePoint.longitude = circle.center.longitude
                positionChanged.connect(resizeCircle)
            }
            onReleased: {
                map.gesture.enabled = true
                positionChanged.disconnect(resizeCircle)
                mainWindow.subscribeRadius = circle.radius
            }
        }
        MapCircle {
            id: circle
            center: mainWindow.subscribePoint
            radius: mainWindow.subscribeRadius
            color: 'cyan'
            border.width: 3
            opacity: 0.5
            visible: false

        }
    }
}
