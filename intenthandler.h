﻿#ifndef INTENTHANDLER_H
#define INTENTHANDLER_H

#include <QAndroidActivityResultReceiver>
#include <QObject>

class ImageCapture;

class IntentHandler : public QAndroidActivityResultReceiver
{
public:
    IntentHandler(ImageCapture *ic);
    void handleActivityResult(int receiverRequestCode, int resultCode, const QAndroidJniObject & data);

private:
    ImageCapture *m_ic;

};

#endif // INTENTHANDLER_H
