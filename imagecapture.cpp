#include <QThread>
#include <QFileDialog>

#include "imagecapture.h"


ImageCapture::ImageCapture(QQuickItem *parent)
    : QQuickItem(parent)
{
}


QString ImageCapture::share()
{
    m_path = QString("");
    m_path = QFileDialog::getOpenFileName(nullptr, "Open File", "", "Images (*.jpg)");
    emit photoTaken();
    return "Photo taken by file browser";
}
