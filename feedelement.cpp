#include "feedelement.h"

FeedElement::FeedElement(QObject *parent) : QObject(parent)
{

}

FeedElement::FeedElement(QString feedId, QString title, QString description, QString photo, QGeoCoordinate point, QString author, QObject *parent)
    : QObject(parent),
      m_feedId(feedId),
      m_title(title),
      m_description(description),
      m_photo(photo),
      m_point(point),
      m_author(author)
{
}
